package com.company;

public class Main {

    public static void main(String[] args) {

        Services service = new Services();

        service.askMinNum();
        service.askMaxNum();
        service.createArray();
        while(true) {
            service.processCommand();
        }
    }
}
