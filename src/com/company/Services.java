package com.company;

import java.util.Random;
import java.util.Scanner;

public class Services {
    private static final String CMD_GENERATE = "generate";
    private static final String CMD_HELP = "help";
    private static final String CMD_EXIT = "exit";
    private static final String SCAN_MIN = "Привет, введи минимальное число диапазона генератора случайных чисел и нажми Enter \"";
    private static final String SCAN_MAX = "Введи максимальное число диапазона генератора случайных чисел и нажми Enter \"";
    private static final String HELPER = "Для генерации случайного числа напишиете - generate.\n" + "Для выхода напишите exit.\nДля помощи напишите help.";
    private static final String BIE_COMMAND = "Вы сгенереровали все доступные числа из указаного диапазона. Пока!!! ";
    private static final String MIN_MAX_AlERT = "Минимальная граница должна быть ровна или больше 1\"Максимальная граница должна быть меньше или ровна 500\" ";


    private final Scanner scanner = new Scanner(System.in);
    private final Scanner scannerLine = new Scanner(System.in);
    private final Random random = new Random();

    int numberOfIndex = 0;
    int min;
    int max;
    int checkedNum;
    int lengthSaveBox;
    int[] arrSaveBox;


    public void askMinNum(){
        System.out.println(SCAN_MIN);
        min = scanner.nextInt();
        if (min < 1 || max > 500 ){
            System.out.println(MIN_MAX_AlERT);
            askMinNum();
        }
    }

    public void askMaxNum(){
        System.out.println(SCAN_MAX);
        max = scanner.nextInt();
        if (min < 1 || max > 500 ){
            System.out.println(MIN_MAX_AlERT);
            askMaxNum();
        }
    }

    public void createArray() {
        lengthSaveBox = (max - min) + 1;
        arrSaveBox = new int[lengthSaveBox];
    }

    public void processCommand() {
        System.out.println("Введите команду");
        String command = scannerLine.nextLine();
        switch (command) {
            case CMD_GENERATE:
                getRandomAndCheckForSimilarity();
                updateSaveBox();
                checkIfFinishTheGame();
                break;
            case CMD_HELP:
                System.out.println(HELPER);
                break;
            case CMD_EXIT:
                System.out.println("Пока!! Программа заканчивает работу");
                System.exit(0);
            default:
                System.out.println("Invalid command! plz try again");
                break;
        }
    }

    private int getRandomNumber() {
        int diff = max - min;
        int randomNumber = random.nextInt(diff + 1);
        return randomNumber += min;
    }

    private void getRandomAndCheckForSimilarity (){
         checkedNum = getRandomNumber();
            for (int i = 0; i < arrSaveBox.length; i++) {
                if (arrSaveBox[i] == checkedNum) {
                    checkedNum = getRandomNumber();
                    i = 0;
                }
            }
        System.out.println("Сгенерировалось число - " + checkedNum);
    }

    private void updateSaveBox(){
        arrSaveBox[numberOfIndex] = checkedNum;
        numberOfIndex = numberOfIndex + 1;
    }

    private void checkIfFinishTheGame(){
        if (numberOfIndex == (arrSaveBox.length) ){
            System.out.println(BIE_COMMAND);
            System.exit(0);
        }
    }
}
